declare namespace PIXI {
    interface Geometry {
        /**
         *
         * Adds an attribute to the geometry
         *
         * @param {String} id - the name of the attribute (matching up to a shader)
         * @param {PIXI.Buffer} [buffer] the buffer that holds the data of the attribute . You can also provide an Array and a buffer will be created from it.
         * @param {Number} [size=0] the size of the attribute. If you have 2 floats per vertex (eg position x and y) this would be 2
         * @param {Boolean} [normalized=false] should the data be normalized.
         * @param {Number} [type=PIXI.TYPES.FLOAT] what type of number is the attribute. Check {PIXI.TYPES} to see the ones available
         * @param {Number} [stride=0] How far apart (in floats) the start of each value is. (used for interleaving data)
         * @param {Number} [start=0] How far into the array to start reading values (used for interleaving data)
         *
         * @return {PIXI.Geometry} returns self, useful for chaining.
         */
        addAttribute(
            id: string,
            buffer?: PIXI.Buffer | number[],
            size?: number,
            normalized?: boolean,
            type?: number,
            stride?: number,
            start?: number,
        ): PIXI.Geometry;
    }

    interface Container {
        /**
         * Adds one or more children to the container.
         *
         * Multiple items can be added like so: `myContainer.addChild(thingOne, thingTwo, thingThree)`
         *
         * @param {...PIXI.DisplayObject} child - The DisplayObject(s) to add to the container
         * @return {PIXI.DisplayObject} The first child that was added.
         */
        addChild<T extends PIXI.DisplayObject>(child: T, ...rest: PIXI.DisplayObject[]): T;
        /**
         * Adds a child to the container at a specified index. If the index is out of bounds an error will be thrown
         *
         * @param {PIXI.DisplayObject} child - The child to add
         * @param {number} index - The index to place the child in
         * @return {PIXI.DisplayObject} The child that was added.
         */
        addChildAt<T extends PIXI.DisplayObject>(child: T, index: number): T;

    }
}
