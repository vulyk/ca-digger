export const renderer = new (class extends PIXI.Renderer {
    constructor() {
        super({
            view: document.getElementById("canvas")! as HTMLCanvasElement,
            // antialias: true,
            backgroundColor: 0x400080,
        });

        window.addEventListener("resize", () => this.fitView());
        this.fitView();
    }

    fitView() {
        this.resize(this.view.clientWidth, this.view.clientHeight);
    }
})();
