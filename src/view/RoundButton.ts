import {requestRender} from "../requestRender";
import {Subject} from "rxjs";
import {graphicsTexture} from "../assets/textures";
import {theme} from "../assets/theme";
import {CacheMap} from "../utils/CacheMap";

export const roundButtonTextures = new CacheMap((r: number) => ({
    enabled: graphicsTexture(g => {
        g.beginFill(theme.cells.normal[2], 1);
        g.lineStyle(4, theme.cells.empty[2]);
        g.drawCircle(0, 0, r);
    }),
    disabled: graphicsTexture(g => {
        g.beginFill(theme.cells.normal[2], 0);
        g.lineStyle(4, theme.cells.empty[2]);
        g.drawCircle(0, 0, r);
    }),
}));

export class RoundButton extends PIXI.Sprite {
    set texture(value: PIXI.Texture) {
        super.texture = value;
        this.anchor.set(0.5);
        requestRender();
    }

    onClick = new Subject();

    constructor(
        public radius: number
    ) {
        super();
        this.anchor.set(0.5);
        this.on("pointertap", () => this.onClick.next());
        this.setInteractive(true);
    }

    buttonMode = true;

    setInteractive(value: PIXI.Sprite["interactive"]) {
        super.interactive = value;
        this.texture =
            value
                ? roundButtonTextures.get(this.radius).enabled
                : roundButtonTextures.get(this.radius).disabled;
    }
}
