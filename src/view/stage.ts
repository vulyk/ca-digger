import {Gui} from "./Gui";
import {pixelsPerCell} from "../assets/textures";
import {PlayerView} from "./PlayerView";
import {UniverseView} from "./UniverseView";
import {renderer} from "../renderer";
import {universe} from "../model/universe";
import {player} from "../model/player";
import {requestRender} from "../requestRender";
import {IntroView} from "./IntroView";

export const stage = new (class extends PIXI.Container {
    camera = this.addChild(new (class extends PIXI.Container{
        constructor() {
            super();

            universe.stateVersion.subscribe(() => {
                function ensure(value: number, min: number, max: number) {
                    return Math.max(min, Math.min(max, value));
                }

                const x = ensure(player.cell.x, 5, universe.spaceSize - 1 - 5);
                const t = ensure(player.cell.t, 5, universe.timeSize - 1 - 5);

                this.pivot.set(x * pixelsPerCell, t * pixelsPerCell);
                requestRender();
            });
        }
    })());
    background = this.camera.addChild(new PIXI.Container());
    cells = this.camera.addChild(new UniverseView());
    player = this.camera.addChild(new PlayerView());
    gui = this.addChild(new Gui());
    intro = this.addChild(new IntroView());

    resize(width: number, height: number) {
        this.position.set(width / 2, height / 2);
        this.camera.scale.set(width / (pixelsPerCell * 20));
        this.camera.position.y = -height * 0.15;
        requestRender();
    }

    fit(el: Element) {
        this.resize(el.clientWidth, el.clientHeight);
    }

    constructor() {
        super();

        window.addEventListener("resize", () => this.fit(renderer.view));
        this.fit(renderer.view);
    }
})();
