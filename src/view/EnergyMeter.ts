import {universe} from "../model/universe";
import {player} from "../model/player";
import {
    hudTextStyle
} from "./hudTextStyle";
import {theme} from "../assets/theme";
import {setTimeoutAsync} from "../utils/misc";
import {requestRender} from "../requestRender";

export class EnergyMeter extends PIXI.Text {
    set text(value: PIXI.Text["text"]) {
        super.text = value;
        requestRender();
    }

    constructor() {
        super("", hudTextStyle.clone());

        universe.stateVersion.subscribe(() => {
            this.text = `Energy: ${player.energy.toString()}`;
        });

        player.insufficientEnergySubject
            .subscribe(() => this.playAttention());
    }

    setColorTheme(key: "normal" | "negative") {
        this.style.fill = theme.text[key].main;
        this.style.stroke = theme.text[key].outline;
        this.style.dropShadowColor = theme.text[key].outline;
        requestRender();
    }

    async playAttention() {
        await setTimeoutAsync(100);
        this.setColorTheme("negative");
        await setTimeoutAsync(100);
        this.setColorTheme("normal");
        await setTimeoutAsync(100);
        this.setColorTheme("negative");
        await setTimeoutAsync(100);
        this.setColorTheme("normal");
    }
}
