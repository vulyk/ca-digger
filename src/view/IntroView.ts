import {tap} from "../utils/misc";
import {requestRender} from "../requestRender";
import {hudTextStyle} from "./hudTextStyle";
import {universe} from "../model/universe";
import {operators, Subject} from "rxjs";
import {arrowTexture, Joystick} from "./Joystick";
import {roundButtonTextures} from "./RoundButton";
import {renderer} from "../renderer";
import {getGuiScale} from "./Gui";

class IntroText extends PIXI.Text {
    set text(value: PIXI.Text["text"]) {
        super.text = value;
        requestRender();
    }

    constructor(text: string) {
        super(text, tap(hudTextStyle.clone(), ts => {
            ts.fontSize = Math.floor((hudTextStyle.fontSize as number) * 2);
            ts.padding = Math.floor(hudTextStyle.padding * 2);
            ts.strokeThickness = Math.floor(hudTextStyle.strokeThickness * 2);
            ts.padding = Math.floor(hudTextStyle.padding * 2);
        }));
        this.anchor.set(0.5);
    }
}

export class IntroView extends PIXI.Container {
    _onResize = new Subject<{ w: number, h: number }>();
    resize() {
        this._onResize.next({w: renderer.screen.width, h: renderer.screen.height});
    };

    constructor() {
        super();

        universe.stateVersion
            .pipe(
                operators.skip(1),
            )
            .subscribe(() => {
                this.visible = false;
                requestRender();
            });

        this._onResize.subscribe(({w, h}) => {
            this.scale.set(getGuiScale(w, h));
            requestRender();
        });
        this.resize();
        window.addEventListener("resize", () => this.resize());
    }

    text1 = tap(this.addChild(new IntroText("Press")), el => {
        el.position.set(0, -100);
    });
    text2 = tap(this.addChild(new IntroText("\"S\" or \"⭣\"")), el => {
        el.position.set(0, 20 * devicePixelRatio);
        el.visible = !("ontouchstart" in document.documentElement);
    });
    text21 = tap(this.addChild(new PIXI.Sprite(arrowTexture)), el => {
        el.position.set(0, 80);
        el.anchor.set(0.5, 0);
        el.alpha = 0.6;
        el.rotation = Math.PI;
        tap(el.addChild(
            new PIXI.Sprite(roundButtonTextures.get(Joystick.dist * 0.3).enabled)), sel => {
            sel.position.set(0, 0.42 * el.texture.height);
            sel.anchor.set(0.5);
        });
        el.visible = ("ontouchstart" in document.documentElement);
    });
    text3 = tap(this.addChild(new IntroText("to dig down")), el => {
        el.position.set(0, 140);
    });
}
