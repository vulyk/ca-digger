import * as textures from "../assets/textures";
import {setTimeoutAsync} from "../utils/misc";
import {player} from "../model/player";
import {universe} from "../model/universe";
import {pixelsPerCell} from "../assets/textures";
import {requestRender} from "../requestRender";

export class PlayerView extends PIXI.Sprite {
    constructor() {
        super(textures.player.normal);
        this.anchor.set(0.5);

        player.onInput
            .subscribe(() => this.playInputFeedback());

        player.insufficientEnergySubject
            .subscribe(() => this.playAttention());

        universe.stateVersion.subscribe(() => {
            this.position.set(
                player.cell.x * pixelsPerCell,
                player.cell.t * pixelsPerCell);
        });
    }

    set texture(value: PIXI.Texture) {
        super.texture = value;
        requestRender();
    }

    async playInputFeedback() {
        this.texture = textures.player.inputFeedback;
        await setTimeoutAsync(100);
        this.texture = textures.player.normal;
    }

    async playAttention() {
        await setTimeoutAsync(100);
        this.texture = textures.player.attention;
        await setTimeoutAsync(100);
        this.texture = textures.player.normal;
        await setTimeoutAsync(100);
        this.texture = textures.player.attention;
        await setTimeoutAsync(100);
        this.texture = textures.player.normal;
    }
}
