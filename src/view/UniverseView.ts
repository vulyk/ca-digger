import {universe} from "../model/universe";
import {graphicsTexture, pixelsPerCell} from "../assets/textures";
import * as textures from "../assets/textures";
import {requestRender} from "../requestRender";
import {player} from "../model/player";
import {tap} from "../utils/misc";

function autocompleteTexture(fillColor: number, strokeColor: number) {
    return graphicsTexture(g => {
        g.lineStyle(2, strokeColor);
        g.beginFill(fillColor);
        g.drawCircle(0, 0, pixelsPerCell / 10);
    });
}

const autocompleteTextures = [
    autocompleteTexture(0xFF0000, 0x400000),
    autocompleteTexture(0x00FF00, 0x004000),
    autocompleteTexture(0x0000FF, 0x000040),
]

class CellView extends PIXI.Sprite {
    set texture(value: PIXI.Sprite["texture"]) {
        super.texture = value;
        requestRender();
    }

    constructor(
        public cellT: number,
        public cellX: number,
    ) {
        super();
        this.anchor.set(0.5);
        universe.stateVersion.subscribe(() => {
            const x = player.cell.x + this.cellX;
            const t = player.cell.t + this.cellT;

            if (x < 0 || x >= universe.spaceSize || t < 0 || t >= universe.timeSize) {
                this.texture = PIXI.Texture.EMPTY;
                return;
            }

            const cell = universe.spacetime[t][x];

            this.texture =
                cell.isEmpty ? textures.emptyCells[cell.state] : textures.cells[cell.state];

            this.autocompleteSprites.forEach((s, i) => {
                if ("undefined" === typeof player.kk()[i]) {
                    s.visible = false;
                    return;
                }
                s.visible = !!player.kk()[i].find(e => e.t === t && e.x === x);
            });
        });
    }

    autocompleteSprites = autocompleteTextures.map((t, i) =>
        tap(this.addChild(new PIXI.Sprite(t)), el => {
            el.anchor.set(0.5);
            el.x = el.texture.width * 1 * (1 - i);
        }));
}

export class UniverseView extends PIXI.Container {
    radius = 30;
    size = this.radius * 2 + 1;
    cells = Array.from({length: this.size}, (_, i) => Array.from({length: this.size}, (__, j) => {
        const t = i - this.radius;
        const x = j - this.radius;
        return tap(this.addChild(new CellView(t, x)), el => {
            universe.stateVersion.subscribe(() => {
                el.position.set(
                    (player.cell.x + x) * pixelsPerCell,
                    (player.cell.t + t) * pixelsPerCell);
            });
        });

    }));
}
