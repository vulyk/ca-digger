import {tap} from "../utils/misc";
import {player} from "../model/player";
import {theme} from "../assets/theme";
import {graphicsTexture} from "../assets/textures";
import {universe} from "../model/universe";
import {RoundButton} from "./RoundButton";

const joystickDist = 140;
export const arrowTexture = graphicsTexture(g => {
    g.beginFill(theme.cells.normal[2]);
    g.lineStyle(4, theme.cells.empty[2]);
    g.moveTo(0, 0);
    g.lineTo(0.6 * joystickDist, -0.52 * joystickDist);
    g.lineTo(0, -0.875 * joystickDist);
    g.lineTo(-0.6 * joystickDist, -0.52 * joystickDist);
    g.closePath();
    g.endFill();
});

class ArrowButtonSprite extends PIXI.Sprite {
    constructor() {
        super();
        this.anchor.set(0.5, 0);
    }

    texture = arrowTexture;

    button = tap(this.addChild(new RoundButton(joystickDist * 0.3)), el => {
        el.position.set(0, 0.42 * this.texture.height);
    });

    get onClick() {
        return this.button.onClick;
    }
}

export class Joystick extends PIXI.Container {
    static dist = joystickDist;

    constructor() {
        super();
    }

    arrowsContainer = tap(this.addChild(new PIXI.Container()), el => {

    });

    arrowButtonUp = tap(this.arrowsContainer.addChild(new ArrowButtonSprite()), el => {
        el.onClick.subscribe(() => player.move("up"));
        el.rotation = 0;
        el.position.set(0, -joystickDist);
    });

    arrowButtonDown = tap(this.arrowsContainer.addChild(new ArrowButtonSprite()), el => {
        el.onClick.subscribe(() => player.move("down"));
        el.rotation = Math.PI;
        el.position.set(0, joystickDist);
    });

    arrowButtonLeft = tap(this.arrowsContainer.addChild(new ArrowButtonSprite()), el => {
        el.onClick.subscribe(() => player.move("left"));
        el.rotation = 3 * Math.PI / 2;
        el.position.set(-joystickDist, 0);
    });

    arrowButtonRight = tap(this.arrowsContainer.addChild(new ArrowButtonSprite()), el => {
        el.onClick.subscribe(() => player.move("right"));
        el.rotation = Math.PI / 2;
        el.position.set(joystickDist, 0);
    });

    button = tap(this.addChild(new RoundButton(joystickDist * 0.3)), el => {
        el.onClick.subscribe(() => player.undo());
        el.position.set(
            -joystickDist - 3 + el.radius,
            -joystickDist * 1.6);

        universe.stateVersion.subscribe(() => {
            el.setInteractive("undefined" !== typeof player.lastMove);
        });
    });
}
