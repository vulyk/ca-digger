import {Universe, universe} from "../model/universe";
import {maxDepthEver, player} from "../model/player";
import {tap} from "../utils/misc";
import {renderer} from "../renderer";
import * as textures from "../assets/textures";
import {EnergyMeter} from "./EnergyMeter";
import {hudTextStyle} from "./hudTextStyle";
import {theme} from "../assets/theme";
import {Joystick} from "./Joystick";
import {BehaviorSubject, Subject} from "rxjs";
import {requestRender} from "../requestRender";
import {graphicsTexture} from "../assets/textures";
import {RoundButton} from "./RoundButton";
import * as rxjs from "rxjs";
import {log} from "../model/log";

const energyLogLineStyle = tap(hudTextStyle.clone(), s => {
    s.fontSize = Math.floor((hudTextStyle.fontSize as number) * 0.6);
    s.padding = Math.floor(hudTextStyle.padding * 0.6);
    s.strokeThickness = Math.floor(hudTextStyle.strokeThickness * 0.6);
    s.padding = Math.floor(hudTextStyle.padding * 0.6);
});

function logToString(log: Array<{
    reason: string,
    value: number,
}>) {
    return `${log[0].reason} ${log[0].value}: `
        + log.filter((_, i) => i > 0).map(l => `${l.reason} ${l.value}`).join(", ");
}

export class EnergyLogLine extends PIXI.Text {
    constructor(
        public lineIndex: number,
    ) {
        super("", energyLogLineStyle.clone());
        this.alpha = 1 - lineIndex / 12;

        log.dataVersion.subscribe(() => {
            const logEntry = log.data[this.lineIndex];
            if (!logEntry) {
                return;
            }

            this.text = logEntry.entry + (logEntry.count > 1 ? ` [${logEntry.count}]` : "");
            this.setColorTheme(logEntry.extra >= 0 ? "normal" : "negative");
        });
    }

    setColorTheme(key: "normal" | "negative") {
        this.style.fill = theme.text[key].main;
        this.style.stroke = theme.text[key].outline;
        this.style.dropShadowColor = theme.text[key].outline;
    }
}

export function getGuiScale(w: number, h: number) {
    return Math.min(w, h) / 650;
}

export class Gui extends PIXI.Container {
    _onResize = new Subject<{ w: number, h: number }>();
    resize() {
        this._onResize.next({w: renderer.screen.width, h: renderer.screen.height});
    };

    _onSwitchGamepadMode = new BehaviorSubject<"hidden" | "lefthand" | "righthand">(
        ("ontouchstart" in document.documentElement) ? "lefthand" : "hidden");

    switchGamepadMode() {
        const nextMode = {
            "hidden": "lefthand" as "lefthand",
            "lefthand": "righthand" as "righthand",
            "righthand": "hidden" as "hidden",
        };
        this._onSwitchGamepadMode.next(nextMode[this._onSwitchGamepadMode.value]);
    }

    constructor() {
        super();

        this.resize();
        window.addEventListener("resize", () => this.resize());
    }

    topLeftDock = tap(this.addChild(new PIXI.Container()), el => {
        this._onResize.subscribe(({w, h}) => {
            el.position.set(-w / 2, -h / 2);
            el.scale.set(getGuiScale(w, h));
        });
    });
    bottomLeftDock = tap(this.addChild(new PIXI.Container()), el => {
        this._onResize.subscribe(({w, h}) => {
            el.position.set(-w / 2, h / 2);
            el.scale.set(getGuiScale(w, h));
        });
    });
    topRightDock = tap(this.addChild(new PIXI.Container()), el => {
        this._onResize.subscribe(({w, h}) => {
            el.position.set(w / 2, -h / 2);
            el.scale.set(getGuiScale(w, h));
        });
    });
    bottomRightDock = tap(this.addChild(new PIXI.Container()), el => {
        this._onResize.subscribe(({w, h}) => {
            el.position.set(w / 2, h / 2);
            el.scale.set(getGuiScale(w, h));
        });
    });
    bottomDock = tap(this.addChild(new PIXI.Container()), el => {
        this._onResize.subscribe(({w, h}) => {
            el.position.set(0, h / 2);
            el.scale.set(getGuiScale(w, h));
        });
    });

    energyText = tap(
        this.topLeftDock.addChild(new EnergyMeter()),
        el => {
            el.position.set(20, 20);
        });

    energyLog = tap(
        this.topLeftDock.addChild(new PIXI.Container()),
        el => {
            el.position.set(20, 60);
        });
    energyLogTexts = Array.from({length: 6}).map((_, i) => tap(
        this.energyLog.addChild(new EnergyLogLine(i)),
        el => el.position.set(0, i * (energyLogLineStyle.fontSize as number) * 1.2)));

    depthText = tap(this.topRightDock.addChild(new (class extends PIXI.Text {
        set text(value: PIXI.Text["text"]) {
            super.text = value;
            requestRender();
        }

        constructor() {
            super("", hudTextStyle.clone());
            this.style.textAlign = "right";
            this.anchor.set(1, 0);
            this.position.set(-20, 20);

            universe.stateVersion.subscribe(() => {
                this.text =
                    `Depth: ${(player.depth).toString()}`
                    + ` / ${(player.maxDepth).toString()}`
                    + ` / ${(maxDepthEver.get()).toString()}`;
            });
        }
    })()));

    seedText = tap(this.bottomRightDock.addChild(new (class extends PIXI.Text {
        constructor() {
            super(
                `Seed: ${Universe.seed.toString()}`,
                hudTextStyle.clone());
            this.style.textAlign = "right";
            this.anchor.set(1, 1);
            this.position.set(-20, -5);
            this.style.fontSize = Math.floor((hudTextStyle.fontSize as number) * 0.4);
            this.style.padding = Math.floor(hudTextStyle.padding * 0.4);
            this.style.strokeThickness = Math.floor(hudTextStyle.strokeThickness * 0.4);
            this.style.padding = Math.floor(hudTextStyle.padding * 0.4);
        }
    })()));

    joystickContainer = tap(this.bottomLeftDock.addChild(new PIXI.Container()), el => {
        el.pivot.set(-Joystick.dist, Joystick.dist);
        el.alpha = 0.6;
        el.position.set(20, -20);

        rxjs.combineLatest([this._onResize, this._onSwitchGamepadMode])
            .subscribe(([{w, h}, gamepadMode]) => {
                requestRender();

                el.visible = gamepadMode !== "hidden";

                if (gamepadMode === "hidden") {
                    return;
                }

                const dock = gamepadMode === "lefthand" ? this.bottomLeftDock : this.bottomRightDock;
                dock.addChild(el);

                const modeFactor = gamepadMode === "lefthand" ? 1 : -1;
                el.scale.x = modeFactor;
                el.position.set(
                    20 * modeFactor,
                    -20,
                );

                this.joystick.arrowsContainer.scale.x = modeFactor;

            });

    });
    joystick = tap(
        this.joystickContainer.addChild(new Joystick()),
    );

    newGameButtonContainer = tap(this.bottomRightDock.addChild(new PIXI.Container()), el => {
        el.alpha = 0.6;

        rxjs.combineLatest([this._onResize, this._onSwitchGamepadMode])
            .subscribe(([{w, h}, gamepadMode]) => {
                requestRender();

                el.visible = gamepadMode !== "hidden";

                if (gamepadMode === "hidden") {
                    return;
                }

                const dock = gamepadMode === "lefthand" ? this.bottomRightDock : this.bottomLeftDock;
                dock.addChild(el);

                const modeFactor = gamepadMode === "lefthand" ? 1 : -1;
                el.scale.x = modeFactor;
                el.position.set(
                    -20 * modeFactor,
                    -20,
                );

            });

    });
    newGameButton = tap(this.newGameButtonContainer.addChild(new RoundButton(Joystick.dist * 0.4)), el => {
        el.anchor.set(1);
        el.onClick.subscribe(() => {
            if (confirm("Start new game? Current progress will be lost")) {
                location.href += "";
            }
        });
    });

    switchHandButtonContainer = tap(this.bottomDock.addChild(new PIXI.Container()), el => {
        el.position.set(0, -20);
        el.alpha = 0.6;
    });
    switchHandButton = tap(
        this.switchHandButtonContainer.addChild(new RoundButton(Joystick.dist * 0.15)),
        el => {
            el.anchor.set(0.5, 1);
            el.onClick.subscribe(() => this.switchGamepadMode());
        });
}
