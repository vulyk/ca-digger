import {theme} from "../assets/theme";

export const hudTextStyle = new PIXI.TextStyle({
    fontSize: 30,
    fill: theme.text.normal.main,
    strokeThickness: 4,
    stroke: theme.text.normal.outline,
    padding: 30,
    dropShadow: true,
    dropShadowColor: theme.text.normal.outline,
    dropShadowDistance: 0,
    dropShadowBlur: 7,
});
