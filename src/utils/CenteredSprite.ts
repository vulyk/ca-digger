export class CenteredSprite extends PIXI.Sprite {
    constructor(texture?: PIXI.Texture) {
        super(texture);
        this.anchor.set(0.5);
    }
}
