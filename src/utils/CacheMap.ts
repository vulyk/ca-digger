export class CacheMap<TKey, TValue> extends Map<TKey, TValue> {
    constructor(
        public create: (key: TKey) => TValue,
    ) {
        super();
    }

    get(key: TKey) {
        if (!super.has(key)) {
            super.set(key, this.create(key));
        }
        return super.get(key)!;
    }
}
