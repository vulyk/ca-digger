export class Pool<T> extends Array<T> {
    constructor(
        public create: () => T,
    ) {
        super();
    }

    pop() {
        if (this.length === 0) {
            return this.create();
        }

        return super.pop()!;
    }
}
