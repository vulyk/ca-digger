import * as assetPaths from "./assetPaths";

export default {
    ...(() => {
        const x: any = {};
        for (const key of Object.keys(assetPaths)) {
            x[key] = PIXI.Loader.shared.resources[key].texture;
        }
        return x as {[key in keyof typeof assetPaths]: PIXI.Texture}
    })()
}
