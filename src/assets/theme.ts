export function toCssColor(color: number) {
    return "#" + color.toString(16).padStart(6, "0");
}

export const theme = new (class {
    cells = {
        normal: [0x8000FF, 0x000000, 0x80FF00],
        empty: [0x201030, 0x102000, 0x204000],
    };
    text = {
        normal: {
            main: toCssColor(this.cells.normal[2]),
            outline: toCssColor(this.cells.empty[2]),
        },
        negative: {
            main: toCssColor(this.cells.normal[0]),
            outline: toCssColor(this.cells.empty[0]),
        }
    };
});
