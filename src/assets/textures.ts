import assets from "./assets";
import {renderer} from "../renderer";
import {theme} from "./theme";

export const pixelsPerCell = 60;

export function graphicsTexture(draw: (g: PIXI.Graphics) => void) {
    const g = new PIXI.Graphics();
    draw(g);
    return renderer.generateTexture(g, PIXI.SCALE_MODES.NEAREST, 1);
}


function cell(color: number) {
    return graphicsTexture(g => {
        g.lineStyle(0);
        g.beginFill(color);
        g.drawRect(0, 0, pixelsPerCell, pixelsPerCell);
    });
}

export const cells = theme.cells.normal.map(cell);
export const emptyCells = theme.cells.empty.map(cell);

export const empty = graphicsTexture(g => {
    g.lineStyle(0, 0x00FF00);
    g.beginFill(0x808080, 0.5);
    g.drawRect(0, 0, pixelsPerCell, pixelsPerCell);
});

export const player = {
    normal: graphicsTexture(g => {
        g.lineStyle(7, theme.cells.normal[2]);
        g.drawCircle(0, 0, pixelsPerCell / 2 - 6);
    }),
    inputFeedback: graphicsTexture(g => {
        g.lineStyle(7, theme.cells.normal[2]);
        g.drawCircle(0, 0, pixelsPerCell / 2 - 4);
    }),
    attention: graphicsTexture(g => {
        g.lineStyle(7, theme.cells.normal[0]);
        g.drawCircle(0, 0, pixelsPerCell / 2 - 6);
    }),
};
