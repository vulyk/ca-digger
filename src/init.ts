import * as assetPaths from "./assets/assetPaths";

PIXI.Loader.shared.baseUrl = "./assets/";
// for (const [name, url] of Object.entries(assetPaths)) {
//     PIXI.Loader.shared.add(name, url);
// }

PIXI.Loader.shared.load(() => {
    // noinspection JSIgnoredPromiseFromCall
    import("./main");
});
