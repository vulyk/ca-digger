import {stage} from "./view/stage";
import {renderer} from "./renderer";
import {universe} from "./model/universe";
import {player} from "./model/player";
import {requestRender} from "./requestRender";

window.addEventListener("keydown", ev => {
    switch (ev.code) {
        case "ArrowLeft": {
            player.move("left");
            break;
        }
        case "KeyA": {
            player.move("left");
            break;
        }
        case "ArrowRight": {
            player.move("right");
            break;
        }
        case "KeyD": {
            player.move("right");
            break;
        }
        case "ArrowUp": {
            player.move("up");
            break;
        }
        case "KeyW": {
            player.move("up");
            break;
        }
        case "ArrowDown": {
            player.move("down");
            break;
        }
        case "KeyS": {
            player.move("down");
            break;
        }
        case "KeyZ": {
            player.undo();
            break;
        }
        case "KeyJ": {
            stage.gui.switchGamepadMode();
            break;
        }
        case "KeyN": {
            if (confirm("Start new game? Current progress will be lost")) {
                location.href += "";
            }
            break;
        }
    }
});

if (!("ontouchstart" in document.documentElement)) {
    stage.gui.joystickContainer.visible = false;
    stage.gui.newGameButtonContainer.visible = false;
}

PIXI.Ticker.shared.add(() => {
    if (requestRender.needsRender) {
        renderer.render(stage);
        requestRender.needsRender = false;
    }
});

if (("undefined" !== typeof env.debug) && env.debug) {
    Object.assign(window, {
        universe,
        player,
        renderer,
        stage,
        requestRender,
    });
}
