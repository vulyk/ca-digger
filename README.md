# Digdeeper [(play)](https://vulyk.bitbucket.io/ca-digger/)

Digdeeper utilizes [3-color Totalistic Cellular Automaton code 1815](http://atlas.wolfram.com/01/02/1815/) 
for procedural cave generation with as minimal interpretation as possible 
to achieve out-of-the-box elementary emergent gameplay 
by harnessing the natural emergence of Wolfram's class 4 behavior of simple computational models, 
defined as "...neither completely stable nor completely chaotic. Localized structures appear and interact in complex ways."

![Screenshot](https://bitbucket.org/vulyk/ca-digger/raw/9172459f77c868cad08065801c6e10986bb79537/screenshot.png)

### Controls

Touchscreen-friendly

* WASD and Arrows - move
* Z - undo last move
* N - start new game
* J - switch onscreen gamepad mode (left, right, off)

### Build and run

```
npm i
npm run build
npm start
```
