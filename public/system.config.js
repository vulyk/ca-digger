SystemJS.config({
    map: {
        "rxjs":
            env.local
                ? "../node_modules/rxjs/bundles/rxjs.umd.js"
                : "https://cdnjs.cloudflare.com/ajax/libs/rxjs/7.0.0-alpha.0/rxjs.umd.min.js",
    }
});
